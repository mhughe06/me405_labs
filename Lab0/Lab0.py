''' @file Lab0.py
This file takes a user inputed number and finds that corresponding term in
the Fibonacci sequence.'''

def CheckInt(st):
    ''' This function determines if the input is an integer or a string and
    returns the result
    @return The result - True for valid integer - False for invalid integer.'''
    try: 
        int(st)
        return True
    except ValueError:
        return False
    

def fib (idx): 
    '''This function takes an input index number and then solves for the
    fibonacci sequence for the nth term defined by the user input.
    @fibnum The result - prints the resulting number when complete.'''
    oldn = 0
    newn = 1
    temp = 0
    if (idx == 0):
        fibnum = 0
    elif (idx == 1):
        fibnum = 1
    else:
        while(idx > 1):
            temp = newn + oldn
            oldn = newn
            newn = temp
            fibnum = temp
            idx = idx - 1
    print(fibnum)
 
## This string will be used     
user_in = input('Please enter an index number: ')

if __name__ == '__main__':
    while(True):
        if (CheckInt(user_in) == True):
            if (int(user_in) >= 0):
                print ('Calculating Fibonacci number at index n = {:}.'.format(user_in))
                fib(int(user_in))
            else:
                print('Invalid Input')                
        else:
            if (user_in == 'exit'):
                print ('Leaving program...')
                break
            else:
                print ('Invalid statement')
                
        
            

        user_in = input('Please enter a new index number or enter "exit" to exit the program: ')
